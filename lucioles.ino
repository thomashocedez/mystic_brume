#include "Adafruit_WS2801.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
#include <avr/power.h>
#endif


uint8_t dataPin  = 6;   // Yellow wire on Adafruit Pixels
uint8_t clockPin = 5;    // Green wire on Adafruit Pixels
bool etat = true;
Adafruit_WS2801 strip = Adafruit_WS2801(6, dataPin, clockPin);


// Limite de lumière (0=pleine lumière / 700=Noir absolu): 
int luminosite_max=400;


void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif
  Serial.begin(9600);
  strip.begin();
  pinMode(10, OUTPUT);
  // Update LED contents, to start they are all 'off'
  strip.show();
    randomSeed(analogRead(A1));

}


void loop() {
  
  int valeur = analogRead(A0);
  Serial.println(valeur);
  if (valeur >= luminosite_max){
    brume();
    if (random(5)==random(5)) bougie();
  } else {
    pas_brume();
  }
    eteindre();
 
  delay(1000);
}

void brume(){
  digitalWrite(10,HIGH);
}
void pas_brume(){
  digitalWrite(10,LOW);
}
void bougie(){
     
     for (int i=00;i<=200;i++){
      for (int j=0;j<=5;j++){
            strip.setPixelColor(j, i+random(30));
      }
      strip.show();
      delay(20);
     }
    
     for (int i=200;i>00;i--){
      for (int j=0;j<=5;j++){
            strip.setPixelColor(j, i+random(30));
      }
      strip.show();
      delay(20);
     }
  
     
 }

void eteindre() {
  for (int i = 0; i <= 10; i++) {
    strip.setPixelColor(i, 0);
  }
  strip.show();
}


